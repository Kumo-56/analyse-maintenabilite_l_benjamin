<?php
use PHPUnit\Framework\TestCase;

class InvoiceTest extends TestCase
{
    public function testAddRental()
    {
        $car = $this->createMock(Car::class);
        $rental = $this->createMock(Rental::class);
        $rental->method('getCar')->willReturn($car);

        $customer = new Customer("John Doe");
        $customer->addRental($rental);

        $reflection = new ReflectionClass($customer);
        $rentals = $reflection->getProperty('_rentals');
        $rentals->setAccessible(true);
        $this->assertCount(1, $rentals->getValue($customer));
    }

    public function testInvoice()
    {
        // Création des vraies stratégies de tarification
        $regularPricing = new RegularPricingStrategy();
        $newModelPricing = new NewModelPricingStrategy();

        // Mocks pour les voitures et les locations
        $car1 = $this->createMock(Car::class);
        $car1->method('calculateAmount')->willReturn($regularPricing->calculateAmount(3)); // Montant calculé par RegularPricingStrategy
        $car1->method('getTitle')->willReturn('Car 1');
        $car1->method('getPricingStrategy')->willReturn($regularPricing);


        $car2 = $this->createMock(Car::class);
        $car2->method('calculateAmount')->willReturn($newModelPricing->calculateAmount(2)); // Montant calculé par NewModelPricingStrategy
        $car2->method('getTitle')->willReturn('Car 2');
        $car2->method('getPricingStrategy')->willReturn($newModelPricing);


        $rental1 = $this->createMock(Rental::class);
        $rental1->method('getCar')->willReturn($car1);
        $rental1->method('calculateRentalAmount')->willReturn(34500.0);// Montant calculé par NewModelPricingStrategy 345.0 €
        $rental1->method('getDaysRented')->willReturn(3);

        $rental2 = $this->createMock(Rental::class);
        $rental2->method('getCar')->willReturn($car2);
        $rental2->method('calculateRentalAmount')->willReturn(39000.0); // Montant calculé par NewModelPricingStrategy 390.0 €
        $rental2->method('getDaysRented')->willReturn(2);

    
        // Création d'un client
        $customer = new Customer("John Doe");
        $customer->addRental($rental1);
        $customer->addRental($rental2);

        // Facture attendue basée sur les montants réels calculés par les stratégies de tarification
        $expectedInvoice = "Rental Record for John Doe\n";
        $expectedInvoice .= "\tCar 1\t345.0\n"; // Montant calculé par RegularPricingStrategy
        $expectedInvoice .= "\tCar 2\t390.0\n"; // Montant calculé par NewModelPricingStrategy
        $expectedInvoice .= "Amount owed is 735.0\n"; // Total des montants
        $expectedInvoice .= "You earned 74 frequent renter points\n"; // Ajusté pour 10% du montant total 10% de 735 + 1 car getDaysRented =2 sur newModelPricing

        // Assertion pour vérifier que la sortie de la méthode invoice est conforme à ce qui est attendu
        $this->assertEquals($expectedInvoice, $customer->invoice());
    }

    public function testInvoiceJsonFormat()
{
    // Création des vraies stratégies de tarification
    $regularPricing = new RegularPricingStrategy();
    $newModelPricing = new NewModelPricingStrategy();

    // Mocks pour les voitures et les locations
    $car1 = $this->createMock(Car::class);
    $car1->method('calculateAmount')->willReturn($regularPricing->calculateAmount(3)); // Montant calculé par RegularPricingStrategy
    $car1->method('getTitle')->willReturn('Car 1');
    $car1->method('getPricingStrategy')->willReturn($regularPricing);

    $car2 = $this->createMock(Car::class);
    $car2->method('calculateAmount')->willReturn($newModelPricing->calculateAmount(2)); // Montant calculé par NewModelPricingStrategy
    $car2->method('getTitle')->willReturn('Car 2');
    $car2->method('getPricingStrategy')->willReturn($newModelPricing);


    $rental1 = $this->createMock(Rental::class);
    $rental1->method('getCar')->willReturn($car1);
    $rental1->method('calculateRentalAmount')->willReturn(34500.0);// Montant calculé par NewModelPricingStrategy 345.0 €
    $rental1->method('getDaysRented')->willReturn(3);

    $rental2 = $this->createMock(Rental::class);
    $rental2->method('getCar')->willReturn($car2);
    $rental2->method('calculateRentalAmount')->willReturn(39000.0); // Montant calculé par NewModelPricingStrategy 390.0 €
    $rental2->method('getDaysRented')->willReturn(2);

    // Création d'un client
    $customer = new Customer("John Doe");
    $customer->addRental($rental1);
    $customer->addRental($rental2);

    // Facture attendue basée sur les montants réels calculés par les stratégies de tarification
    $expectedJsonInvoice = [
        'customer' => 'John Doe',
        'rentals' => [
            ['title' => 'Car 1', 'amount' => 345.0], // Montant calculé par RegularPricingStrategy
            ['title' => 'Car 2', 'amount' => 390.0]  // Montant calculé par NewModelPricingStrategy
        ],
        'totalAmount' => 735.0, // Total des montants
        'frequentRenterPoints' => 74 // Ajusté pour 10% du montant total 10% de 735 + 1 car getDaysRented =2 sur newModelPricing
    ];

    // Appel de la méthode invoice avec le format JSON
    $jsonInvoice = $customer->invoice('json');

    // Vérification que le JSON généré est équivalent à ce qui est attendu
    $this->assertJsonStringEqualsJsonString(json_encode($expectedJsonInvoice), $jsonInvoice);
}

}
