# analyse-maintenabilite_l_benjamin

## Table des matières

1. [Exercice 1 - Questions sur les acquis notions vues en cours](#exercice-1---questions-sur-les-acquis-notions-vues-en-cours)
    1. [Quelles sont les principales sources de complexité dans un système logiciel ?](#quelles-sont-les-principales-sources-de-complexité-dans-un-système-logiciel-)
    2. [Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ?](#quels-avantages-procure-le-fait-de-programmer-vers-une-interface-et-non-vers-une-implémentation-)
    3. [Compréhension de l'heuristique d'Eric Steven Raymond](#compréhension-de-lheuristique-deric-steven-raymond)
    4. [Méthode ou approche recommandée pour le refactoring](#méthode-ou-approche-recommandée-pour-le-refactoring)
2. [Exercice 2](#exercice-2)

## Exercice 1 - Questions sur les acquis notions vues en cours

### 1. Quelles sont les principales sources de complexité dans un système logiciel ?

Les principales sources de complexité sont l'état du système (gestion des données et de leur cohérence), le flot de contrôle (ordre d'exécution et gestion des chemins multiples) et le volume de code (nombre de lignes et densité des fonctionnalités).

### 2. Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ? Vous pouvez illustrer votre réponse avec un code source minimal et/ou avec un diagramme

Programmer vers une interface permet une plus grande modularité, une meilleure testabilité et une réutilisabilité accrue. Cela facilite aussi le remplacement ou la modification des implémentations sans affecter les autres parties du système.

**Exemple de code :**
```java
interface Animal {
    void seDeplacer();
}

class Dog implements Animal {
    public void seDeplacer() {
        System.out.println("Marche");
    }
}

class Fish implements Animal {
    public void seDeplacer() {
        System.out.println("Nage");
    }
}

public class Main {
    public static void main(String[] args) {
        Animal myDog = new Dog();
        Animal myCat = new Cat();
        myDog.seDeplacer();
        myCat.seDeplacer();
    }
}
```

### 3. Eric Steven Raymond, figure du mouvement open-source et hacker notoire, a récemment écrit sur le développement d’un système : “First make it run, next make it correct, and only after that worry about making it fast.” que l’on peut traduire par “D’abord, faites en sorte que ça fonctionne, ensuite assurez-vous que ce soit correct, et seulement après, préoccupez-vous de le rendre rapide.”. Comment comprenez-vous chaque étape de cette heuristique ? Comment comprenez-vous cette heuristique dans son ensemble ?

-   "First make it run" : L'objectif initial est de créer une solution fonctionnelle, même si elle n'est pas parfaite ou optimisée. Cela permet de valider la viabilité du concept de base.
-   "Next make it correct" : Une fois que le système fonctionne, il faut s'assurer qu'il respecte les exigences, éliminer les bugs et garantir la robustesse.
-   "Only after that worry about making it fast" : La dernière étape consiste à optimiser les performances. Il est inutile d'optimiser un système qui n'est ni fonctionnel ni correct.
Cette heuristique suggère une approche itérative et progressive du développement logiciel, où chaque étape construit sur la précédente, minimisant les risques de régression et assurant une solution solide avant l'optimisation.

### 4. Nous avons vu en cours une technique pour lutter contre la déstructuration du code au cours du temps, le refactoring. Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?

Pour mener à bien un refactoring, il est recommandé de procéder par petits pas, d'utiliser des tests automatisés pour vérifier les changements, de faire des revues de code, de mettre à jour la documentation et d'utiliser des outils de refactoring intégrés dans les IDEs (ex: sonarlint).

## Exercice 2 - Refactoring

### 1. Cloner le dépôt
```bash
git clone https://gitlab.com/Kumo-56/analyse-maintenabilite_l_benjamin.git
cd analyse-maintenabilite_l_benjamin
```
### 2. Lancer le projet
Avec votre installation locale de PHP
Ou 
Avec une version dockerisée de PHP:
```bash
docker run --rm -v $(pwd)/invoice.php:/app/invoice.php -w /app php:8.2-cli php invoice.php
```