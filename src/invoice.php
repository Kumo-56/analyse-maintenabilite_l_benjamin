<?php

class Car
{
    private PricingStrategy $_pricingStrategy;

    private string $_title;

    public function __construct(string $title, PricingStrategy $pricingStrategy)
    {
        $this->_title = $title;
        $this->_pricingStrategy = $pricingStrategy;
    }

    public function getTitle(): string
    {
        return $this->_title;
    }

    public function getPricingStrategy(): PricingStrategy
    {
        return $this->_pricingStrategy;
    }

    public function calculateAmount(int $daysRented): float
    {
        return $this->_pricingStrategy->calculateAmount($daysRented);
    }

}

class Rental
{
    private Car $_Car;
    private int $_daysRented;

    public function __construct(Car $Car, int $daysRented)
    {
        $this->_Car = $Car;
        $this->_daysRented = $daysRented;
    }

    public function getDaysRented(): int
    {
        return $this->_daysRented;
    }

    public function getCar(): Car
    {
        return $this->_Car;
    }

    public function calculateRentalAmount(): float
    {
        return $this->_Car->calculateAmount($this->_daysRented);
    }
}

class Customer
{
    private string $_name;
    private array $_rentals = [];

    public function __construct(string $name)
    {
        $this->_name = $name;
    }

    public function addRental(Rental $arg): void
    {
        $this->_rentals[] = $arg;
    }
    
    public function getRentals(): array
    {
        return $this->_rentals;
    }

    public function getName(): string
    {
        return $this->_name;
    }

    public function invoice(string $format = 'text'): string
    {
        $invoiceGenerator = new InvoiceGenerator();
        return $invoiceGenerator->generate($this, $format);
    }
}

class InvoiceGenerator
{
    const TAUX_CALCUL_POINT_FIDELITE = 10;
    public function generate(Customer $customer, string $format = 'text'): string
    {
        $invoiceData = $this->collectInvoiceData($customer);

        if ($format === 'json') {
            return $this->formatJsonInvoice($invoiceData);
        }
        return $this->formatTextInvoice($invoiceData);
    }

    /**
     * Collects data to generate an invoice
     * @param Customer $customer
     * @return array ['customer' => string, 'rentals' => array, 'totalAmount' => float, 'frequentRenterPoints' => int]
     */
    private function collectInvoiceData(Customer $customer): array
    {
        $totalAmount = 0.0;
        $frequentRenterPoints = 0;
        $rentals = [];

        foreach ($customer->getRentals() as $each) {
            $thisAmount = $each->calculateRentalAmount();

            // Calculate frequent renter points
            $frequentRenterPoints += $this->calculateFrequentRenterPoints($thisAmount/100);

             // Add bonus for a two day new release rental
            if ($each->getCar()->getPricingStrategy() instanceof NewModelPricingStrategy && $each->getDaysRented() > 1) {
                $frequentRenterPoints++;
            }

            $rentals[] = [
                'title' => $each->getCar()->getTitle(),
                'amount' => $thisAmount / 100
            ];
            $totalAmount += $thisAmount;
        }

        return [
            'customer' => $customer->getName(),
            'rentals' => $rentals,
            'totalAmount' => $totalAmount / 100,
            'frequentRenterPoints' => $frequentRenterPoints
        ];
    }

    private function formatTextInvoice(array $invoiceData): string
    {
        $result = "Rental Record for " . $invoiceData['customer'] . "\n";
        foreach ($invoiceData['rentals'] as $rental) {
            $result .= "\t" . $rental['title'] . "\t" . number_format($rental['amount'], 1) . "\n";
        }
        $result .= "Amount owed is " . number_format($invoiceData['totalAmount'], 1) . "\n";
        $result .= "You earned " . $invoiceData['frequentRenterPoints'] . " frequent renter points\n";

        return $result;
    }

    private function formatJsonInvoice(array $invoiceData): string
    {
        return json_encode($invoiceData, JSON_PRETTY_PRINT);
    }

    private function calculateFrequentRenterPoints(float $amount): int
    {
        // Calculate points as 10% of the rental amount
        return (int) ($amount / self::TAUX_CALCUL_POINT_FIDELITE);
    }
}

interface PricingStrategy
{
    public function calculateAmount(int $daysRented): float;
}

class RegularPricingStrategy implements PricingStrategy
{
    public function calculateAmount(int $daysRented): float
    {
        $amount = 5000 + $daysRented * 9500;
        if ($daysRented > 5) {
            $amount -= ($daysRented - 2) * 10000;
        }
        return $amount ;
    }
}

class NewModelPricingStrategy implements PricingStrategy
{
    public function calculateAmount(int $daysRented): float
    {
        $amount = 9000 + $daysRented * 15000;
        if ($daysRented > 3) {
            $amount -= ($daysRented - 2) * 10000;
        }
        return $amount;
    }
}

// Exemples d'utilisation :
$regularPricing = new RegularPricingStrategy();
$newModelPricing = new NewModelPricingStrategy();

$Car1 = new Car("Car 1", $regularPricing);
$Car2 = new Car("Car 2", $newModelPricing);

$rental1 = new Rental($Car1, 3);
$rental2 = new Rental($Car2, 2);

$customer = new Customer("John Doe");
$customer->addRental($rental1);
$customer->addRental($rental2);

echo $customer->invoice();
